# 5'/3' imbalance detection in gene fusion predictions

This repository contains the scripts used to indicate the presence of gene fusion events for pediatric cancer patients, by looking a t the 5'/3' imbalance in expression.

---

## Motivation

This project was done to see if the presence of gene fusion predictions from STAR-Fusion could be indicated when looking at the 5'/3' imbalance. STAR-Fusoin predicts a lot of false positive events, so it was seen for which fusions an imbalance was present between the 5' and 3' parts of at least one of the partner genes. This was done to see if the presence of the imbalance could indicate the presence of the gene fusions.


## Usage of the programs

R was used for the analysis, version 3.6.3. The Rmd files in this repository have a knitted html file that contains the results of the Rmd file.


### Clone the project

To get all files, the repository needs to be cloned.

`git clone https://carlijnruijter@bitbucket.org/c-ruijter/5-3-imbalance-detection.git`


### Installation

To be able to use the scripts, some packages need to be downloaded.
```
install.packages("dplyr")
install.packages("data.table")
install.packages("purrr")
install.packages("ggplot2")
install.packages("ggpubr")
install.packages("kableExtra")
install.packages("rmarkdown")
install.packages("stringr")
install.packages("base")
```

### Data folders

The paths to the data used are in `sources/data_paths.R`. To be able to use this file, the data paths need to be changed to your own personal data paths on your system. Different sets of data are used. For example, new expression data was generated, giving multiple sets of expression data. Make sure to set the right one for your preference.


## Usage of files

`sources/`

The sources folder contains the files where data paths are defined and functions are defined. The functions are used in all files except for the log files. The data paths to the data files need to be changed before running the files.

`transcript_selection/`

The imbalance_sf file was used to create visualize the exon expression of the clinically relevant genes and some possible cancer driver genes. For the visualizations, the transcript ID predicted by STAR-Fusion was used. When there was no transcript ID prediction, they fusion was not visualized.

Because not all fusions had a transcript ID, some selection methods were applied to select a transcript ID. For every partner gene, the transcript with the highest total sum of expression is visualized and the one with the highest mean expression per exon. The results are in `exon_imbalance_visualizations/fusions_sum_fpkm.html` and `exon_imbalance_visualizations/fusions_mean_fpkm.html`, respectively.

`multiple_transcripts_selected/`

The comparison of multiple transcripts per gene is shown in `imbalance_mult_trns.html`. The `imbalance_mult_trns_match.html` reduced the selection by only taking into account the transcripts that had an exon boundary match with the breakpoint from the fusions partner gene.

`transcript_comparison/`

To compare all transcripts, the imbalance ratios of all possible transcripts for all fusion predictions were calculated and saved per patient.

`selected_fusions/`

The results of all imbalance ratios for all transcripts showed some difference in results for the same gene. A few genes were selected for which the range in imbalance ratio was wide for the transcripts. The transcripts with the highest and lowest imbalance ratios are visualized.

This folder also contains a file, `imbalance_expected_genes.Rmd` which was used to analyze the significance of difference in expression of some partner genes, for which imbalance was expected. The analysis was also performed on the patients without the fusion to see if they could also have a significant difference in expression between the 5' and 3' parts. The data used for these genes was combined by the `subset_gene_exp.Rmd` file. This file can be found in the main folder of this repository. It combined the expression of one given gene from all patients.

`calc_imb_ratios_stat/`

To see if the difference in expression between the 5' and 3' parts of the partner genes of the fusion was significant, a Wilcoxon test was applied.

In `calc_all_imb_ratios.R` the code can be found to calculate the imbalance ratios of all predicted fusions with a transcript ID prediction, for all patients.

The `log` files contain the exploration that was done during this project. This includes exploring the data in the beginning and analyzing the results at the end.

## Contact information
Carlijn Ruijter
c.ruijter@st.hanze.nl

## License
MIT &copy; Carlijn Ruijter
